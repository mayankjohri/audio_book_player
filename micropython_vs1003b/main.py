# pico.py VS1053 demo of asynchronous driver running on a RP2 Pico.
# Play all MP3 tracks found in a directory in the breakout board's SD card.

from machine import SPI, Pin, I2C, ADC
import sdcard
import uos
import sys

from vs_1053 import *
import uasyncio as asyncio
from primitives import Switch
from oled import Write, GFX, SSD1306_I2C
from oled.fonts import ubuntu_condensed_12, ubuntu_mono_15, ubuntu_mono_20

DISPLAY_WIDTH=49

playing = False
current_loc = 0

def show_text(txt, x_cord, y_cord, show=True, bgcolor=0, color=1):
    print(txt)
    write12.text(txt, x_cord, y_cord,  bgcolor=bgcolor, color=color)
    if show:
        oled.show()

def title():
    oled.fill(0)
    msg = "Maya AB - 0.0.3"
    show_text(f"{msg:{DISPLAY_WIDTH}}",0, 0, show=False, bgcolor=1, color=0)

scl = Pin(27)
sda = Pin(26)

i2c = I2C(1, scl=scl, sda=sda)

Pin(16, Pin.OUT, value=1)

print("Initializing the OLED")
oled = SSD1306_I2C(128, 64, i2c)
# gfx = GFX(128, 64, oled.pixel)
print("OLED Initializing - Done")
write12 = Write(oled, ubuntu_condensed_12)
title()
oled.fill(0) # clear the display
show_text("Starting ...",2,25)

spi=SPI(1, baudrate=40000000,
        sck=Pin(10),
        mosi=Pin(11),
        miso=Pin(12))

xdcs = Pin(2, Pin.OUT, value=1)  # Data chip select xdcs in datasheet
xcs = Pin(14, Pin.OUT, value=1)  # Labelled CS on PCB, xcs on chip datasheet
dreq = Pin(3, Pin.IN)  # Active high data request
reset = Pin(15, Pin.OUT, value=1)  # Active low hardware reset
sdcs = Pin(13, Pin.OUT, value=1)  # SD card CS


# print("Initialising sdcard")
# sd = sdcard.SDCard(spi, sdcs)
# print("Mounting sdcard")
# uos.mount(sd, '/fc')
# print("Mounted sdcard")
# print(uos.listdir('/fc'))


show_text("Initialising vs1053 board", 0, 15)
player = VS1053(spi, reset, dreq, xdcs, xcs, sdcs, '/fc')
print("initialised")
show_text("Initialising vs1053 board", 0, 15, 0)
show_text("Started", 10, 35)
# player.patch()  # Optional for FLAC playback. From /fc/plugins

can = Pin(16, Pin.IN, Pin.PULL_UP)  # Link pin 16 to gnd to cancel play

async def heartbeat():
    print("test")
    led = Pin(25, Pin.OUT)
    while(True):
        led(not(led()))
        await asyncio.sleep_ms(500)

async def progress(fp):
    val = fp.tell()
    while(True):
        write12.text(val, 0, 10,  0xffff)
        val = fp.tell()
#         oled.fill_rect(0, 10, 132, 132, 0)
        write12.text(val,0, 10)
        oled.show()
        await asyncio.sleep_ms(500)

def move_up():
    pass

def start_menu():
    title()
    print("inside start_menu")
    menu_items = [
        "Play Audiobooks",
        "Read text files (todo)",
        "Power off (todo)"
        ]
    x_cord = 2
    y_cord = 20
    for val in menu_items:
        write12.text(val, x_cord, y_cord)
        y_cord += 13
    oled.show()
    asyncio.sleep_ms(500)

async def display_screen(current_state=0, data=None):
    """
    States:
    0 - Original State
    """
    while True:
        print("test")
        if current_state == 0:
            start_menu()
        elif current_state == 1:
            pass
        await asyncio.sleep_ms(500)
# async def cancel_song():  # Handle cancellation
#     sw = Switch(can)
#     sw.close_func(None)
#     while True:
#         sw.close.clear()
#         await sw.close.wait()
#         print('Cancelling playback')
#         await player.cancel()
#         print('Cancelled')

async def main(locn, cancel):
    asyncio.create_task(heartbeat())
    await  display_screen(0)
#     asyncio.create_task(cancel_song())
#     if cancel:
#         asyncio.create_task(can_it())
#     player.volume(-10, -10)  # -10dB (0dB is loudest)
#     # player.mode_set(SM_EARSPEAKER_LO | SM_EARSPEAKER_HI)  # You decide.
#     # player.response(bass_freq=150, bass_amp=15)  # This is extreme.
#     songs = sorted([x[0] for x in os.ilistdir(locn) if x[1] != 0x4000])
#     while True:
#         for song in songs:
#             print(song)
#             file_name = '/'.join((locn, song))
#             print(file_name)
#             with open(file_name, 'rb') as f:
#                 asyncio.create_task(progress(f))
#                 await player.play(f)  # Ends or is cancelled
#                 await asyncio.sleep(1)  # Gap between tracks
#     print('All done.')

asyncio.run(main('/fc/audio_books/AP and the PS', False))
