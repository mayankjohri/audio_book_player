"""
ffmpeg -i test1.mp3 -af "aformat=sample_fmts=s16:sample_rates=22000" m001.mp3
"""
from board import *
from time import sleep

import board
import audiomp3
import audiobusio
import audiomixer
import time
import busio
import sdcardio
import storage
import os
import gc
from digitalio import DigitalInOut, Pull
import keypad
import displayio

# import terminalio
# import time
import asyncio

# from lib.adafruit_debouncer import Debouncer

import adafruit_ssd1306
from audiomp3 import MP3Decoder

try:
    from audioio import AudioOut
except ImportError:
    try:
        from audiopwmio import PWMAudioOut as AudioOut
    except ImportError:
        pass  # not always supported by every board!


AUDIOBOOK_ROOTPATH = "/sd/audio_books"


def mount_sdcard(mount_folder="/sd"):
    sleep(2)
    print("Mounting the SD Card")
    try:
        spi = busio.SPI(GP10, MOSI=GP11, MISO=GP8)
        sd = sdcardio.SDCard(spi, GP9)

        vfs = storage.VfsFat(sd)
        storage.mount(vfs, mount_folder, readonly=True)
    except Exception as e:
        print(f"Error while mounting: {e}")
        return False, None

    print("SD Card is now mounted")
    return True, vfs


def unmount_sdcard(vfs, mount_folder="/sd"):
    print("Unmounting the SD Card")
    try:
        storage.unmount(mount_folder, vfs)
    except Exception as e:
        print(f"Error while mounting: {e}")
        return False
    print("SD Card is now mounted")
    return True


def split_by_n(seq, n):
    """A generator to divide a sequence into chunks of n units."""
    while seq:
        yield seq[:n]
        seq = seq[n:]


def get_audiobooks():
    books = []
    for entry in os.listdir(AUDIOBOOK_ROOTPATH):
        name = f"{AUDIOBOOK_ROOTPATH}/{entry}"

        if os.stat(name)[0] == 16384:
            books.append(entry)
    return books


def get_mp3_files(path):
    files = []
    folder = f"{AUDIOBOOK_ROOTPATH}/{path}"
    print(f"folder: {folder}")
    for entry in os.listdir(folder):
        if os.stat(name := f"{folder}/{entry}")[0] != 16384 and name.endswith(".mp3"):
            files.append(entry)

    files.sort()
    return files


class Books:
    def __init__(self, display, list_books=None, step=5):
        if not list_books:
            self.refresh_books()
        else:
            self.book_list = list_books
        self.count = len(self.book_list)
        self.current = 0
        self.step = step
        self.display = display

        self.display_audiobooks()

    def refresh_books(self):
        self.book_list = get_audiobooks()

    def get_current_book(self):
        return self.book_list[self.current]

    def display_audiobooks(self):
        self.display.fill(0)

        n = 1
        flag = False
        print(self.book_list[self.current : self.current + self.step])
        for name in self.book_list[self.current : self.current + self.step]:
            col = 1
            if not flag:
                display.rect(0, 0, 127, 10, 1, fill=1)
                col = 0
                flag = True
            self.display.text(name.strip(), x=0, y=n, size=1, color=col)
            n += 10
        self.display.show()

    def move(self, direction):
        if direction == "up" and self.current > 0:
            self.current -= 1
        elif direction == "down" and self.current <= (self.count - 2):
            self.current += 1
        else:
            return
        print(self.current, self.count)
        self.display_audiobooks()


class AudioBook(Books):
    def __init__(self, display, book_name):
        gc.collect()
        self.audio = audiobusio.I2SOut(
            bit_clock=board.GP0, word_select=board.GP1, data=board.GP2
        )
        self.display = display
        print(book_name)
        self.name = book_name
        self.book_list = self.get_files(book_name)

        super().__init__(display, self.book_list)
        self.display_audiobooks()
        self.exit = False
        self.text_area = ""
        self.size = 0

    def __del__(self):
        self.audio.deinit()
        super().__del__()

    def get_files(self, book_name):
        print("getting list of mp3 files")
        # lst = [".."]
        lst = get_mp3_files(book_name)
        print(f"{lst}")
        return lst

    # def move(self, direction):
    #     print("not implemented")

    def quit(self):
        gc.collect()
        self.exit = True

    def display_progress(self, mp3file, file_name, size):
        current_loc = mp3file.tell()
        self.display.fill(0)
        self.display.text(file_name, x=0, y=0, size=1, color=1)
        percent = round(current_loc * 100 / size, 2)
        progress = int(current_loc * 100 / size)
        self.display.text(f"{current_loc} :{percent}%", x=0, y=15, size=1, color=1)
        self.display.show()

    async def play_book(self):

        print("Inside play_book")
        file_name = self.book_list[self.current]

        self.display.fill(0)
        text = ""   # f"{current_loc} of {size} running"
        self.display.text(text.strip(), x=10, y=35, size=1, color=0xFFFF00)

        filename = f"{AUDIOBOOK_ROOTPATH}/{self.name}/{file_name}"
        print(f"opening file: {filename}")

        size = os.stat(filename)[-1]
        print(size)
        print(dir(self.audio))
        with open(filename, "rb") as mp3file:
            #  self.display.text(file_name.strip(),x=0, y=0, size=1, color=0xFFFF00)
            if self.exit:
                self.exit = False
                self.audio.stop()
                return
            #  text_area.color=0x000000
            gc.collect()
            display.show()
            print(f"Playing {filename}")
            decoder = MP3Decoder(mp3file)
            print(dir(MP3Decoder))
            decoder.file = mp3file

            with audiomixer.Mixer(
                sample_rate=decoder.sample_rate, buffer_size=24992
            ) as mixer:
                mixer.voice[0].level = 0.3
                self.audio.play(mixer)
                mixer.voice[0].play(decoder, loop=False)

                mixer.voice[0].level = 0.01
                print("playing", filename)

                self.mixer = mixer
                t1 = time.monotonic()
                # This allows you to do other things while the audio plays!
                while self.audio.playing:
                    gc.collect()
                    t2 = time.monotonic()
                    if time_diff(t1, t2):
                        self.display_progress(mp3file, file_name, size)
                        t1 = time.monotonic()
                    # await catch_pin_transitions()
                    print("action: ", action)
                    if action == "pause":
                        print("Lets pause the execution")
                        self.pause()
                        state = State.pause
                    await asyncio.sleep(1)
            print("done playing")

    def stop(self):
        print("Stoping the song")
        self.audio.stop()

    def pause(self):
        print("Pausing the song")
        self.audio.pause()

    def resume(self):
        print("Pausing the song")
        self.audio.resume()


def get_display():
    displayio.release_displays()
    # This RPi Pico way to call I2C
    i2c = busio.I2C(scl=board.GP15, sda=board.GP14, frequency=200_000)
    return adafruit_ssd1306.SSD1306_I2C(128, 64, i2c)


def time_diff(t1, t2, needed_diff=2):
    delta = t2 - t1
    return True if delta > needed_diff else False


def logme(msg):
    import time

    def _format_datetime(datetime):
        return "{:02}/{:02}/{} {:02}:{:02}:{:02}".format(
            datetime.tm_mon,
            datetime.tm_mday,
            datetime.tm_year,
            datetime.tm_hour,
            datetime.tm_min,
            datetime.tm_sec,
        )

    unix_time = time.time()  # Wed Aug 17 2022 19:36:10 GMT+0000
    tz_offset_seconds = -14400  # NY Timezone

    get_timestamp = int(unix_time + tz_offset_seconds)
    current_unix_time = time.localtime(get_timestamp)
    current_struct_time = time.struct_time(current_unix_time)
    current_date = "{}".format(_format_datetime(current_struct_time))

    print(f"{current_date} - {msg}")


async def perform_action():
    while True:
        global action
        global state
        global books
        global ab
        if action:
            print(action, state, State.start, action)

            print(f"checking state: start: {state == State.start}")
            print(f"checking state: running: {state == State.running}")
            print(f"checking state: pause: {state == State.pause}")
            print(f"checking state: select_ab: {state == State.select_ab}")

            if state == State.start:
                if action in ("up", "down"):
                    print(f"moving {action} in {state}")
                    books.move(action)
                if action == "enter":
                    # Lets play the audio book
                    if ab:
                        del ab
                    ab = AudioBook(display, books.get_current_book())
                    state = State.select_ab
            elif state == State.select_ab:
                print(f"action: {action}")
                if action in ("up", "down"):
                    print("in select ab, up/down")
                    ab.move(action)
                elif action == "enter":
                    # Lets play the audio book
                    if ab.get_current_book() == "..":
                        print("Back")
                        state = State.start
                        if ab:
                            del ab
                            ab = None
                        books.display_audiobooks()
                    else:
                        state = State.running
                        action = None
                        await ab.play_book()
                        print("Running the audiobook")
                elif action == "poweroff":
                    if ab:
                        ab.stop()
                        ab = None
                    books.display_audiobooks()
            elif state == State.running:
                if action in ("up", "down"):
                    print("in select ab, up/down")
                    ab.move(action)
                elif action == "enter":
                    # Lets play the audio book
                    state = State.running
                    await ab.play_book()
                elif action == "pause":
                    if ab:
                        print("Pausing")
                        ab.pause()
                        state = State.pause
                    # books.display_audiobooks()
                elif action == "stop":
                    if ab:
                        print("stopping")
                        ab.stop()
                        state = State.select_ab
                        ab = None
                    books.display_audiobooks()
            elif state == State.pause:
                if action in ("up", "down"):
                    print("in select ab, up/down")
                    # ab.move(action)
                elif action == "enter":
                    pass
                    # Lets play the audio book
                    # state = State.running
                    # await ab.play_book()
                elif action == "resume":
                    if ab:
                        print("resume")
                        ab.resume()
                        state = State.running

                elif action == "ab_list":
                    if ab:
                        print("stopping")
                        ab.stop()
                        state = State.select_ab
                        ab = None
                    books.display_audiobooks()
            print(action, state)
        action = None
        await asyncio.sleep(0)


async def catch_pin_transitions():
    """
    | State      | B1      | B2     | B3               | B4         |
    |------------|---------|--------|------------------|------------|
    | Start      | up      | down   | poweroff (TODO)  | enter      |
    | select_ab  | up      | down   | Back to start    | enter      |
    | Running    | Forward | Back   | Stop/Select_ab   | Pause      |
    | Pause      | forward | Back   | select_ab        | Resume     |
    """
    global state
    global action
    global ab
    """Print a message when pin goes low and when it goes high."""
    pin = [board.GP5, board.GP4, board.GP3, board.GP6]
    with keypad.Keys(pin, value_when_pressed=True) as keys:
        while True:
            event = keys.events.get()
            if event and event.released:
                print(event.key_number, "-", event.timestamp, "-", state)
                print(f"State Running: {state == State.running}")

                if state in (State.start, State.select_ab):
                    print("In state start or select_ab")
                    if event.key_number == 0:
                        action = "up"
                    elif event.key_number == 1:
                        action = "down"
                    elif event.key_number == 2:
                        action = "poweroff"
                    elif event.key_number == 3:
                        action = "enter"
                elif state == State.running:
                    print("In state running")
                    if event.key_number == 0:
                        action = "forward"
                    elif event.key_number == 1:
                        action = "backward"
                    elif event.key_number == 2:
                        action = "stop"
                    elif event.key_number == 3:
                        print("Pausing the execution")
                        ab.pause()
                        action = "None"
                        state = State.pause
                elif state == State.pause:
                    print("In state Pause")
                    if event.key_number == 0:
                        action = "forward"
                    elif event.key_number == 1:
                        action = "backward"
                    elif event.key_number == 2:
                        action = "ab_list"
                    elif event.key_number == 3:
                        action = None
                        ab.resume()
                        state = State.running
                else:
                    action = None
            await asyncio.sleep(0)


class State(object):
    start = 0
    select_ab = 1
    running = 2
    pause = 3


async def main():
    get_button_states = asyncio.create_task(catch_pin_transitions())
    actions = asyncio.create_task(perform_action())
    await asyncio.gather(get_button_states)
    print("Lets wait for buttons")
    # await asyncio.gather(*tasks)  # Don't forget "await"!
    print("done")


state = None
action = None
ab = None

if __name__ == "__main__":
    flg, vfs = mount_sdcard()

    if flg:
        display = get_display()
        display.rotate(180)

        books = Books(display)
        start = 0
        step = 5
        state = State.start
        ab = None
        asyncio.run(main())
