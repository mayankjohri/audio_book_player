# import asyncio
# import board
# import countio
# 
# async def catch_interrupt(pin):
#     """ Print a message when pin goes low."""
#     with countio.Counter(pin) as interrupt:
#         while True:
#             if interrupt.count > 0:
#                 interrupt.count = 0
#                 print(f"interrupted! - {pin}")
#             # Let another task run.
#             await asyncio.sleep(0)
# 
# 
# async def main():
#     interrupt_task1 = asyncio.create_task(catch_interrupt(board.GP5))
#     interrupt_task2 = asyncio.create_task(catch_interrupt(board.GP3))
#     await asyncio.gather(interrupt_task1, interrupt_task2 )
# 
# asyncio.run(main())

import asyncio
import board
import keypad
from time import sleep


def sleeping(pin):
    print(f"Starting sleep - {pin}")
    sleep(1)
    print(f"Ending sleep - {pin}")
    
async def catch_pin_transitions():
    """Print a message when pin goes low and when it goes high."""
    pin = [board.GP5, board.GP26, board.GP3, board.GP4]
#     print(pin)
    with keypad.Keys(pin, value_when_pressed=True) as keys:
        while True:
            
            event = keys.events.get()
#             print(event, "*")
            if event:
                print(event.key_number, event.timestamp, "-")
                if event.pressed:
                    print("pin went low")
                    sleeping(pin)
                elif event.released:
                    print("pin went high")
            await asyncio.sleep(0)

async def main():
    interrupt_task1 = asyncio.create_task(catch_pin_transitions())
#     interrupt_task2 = asyncio.create_task(catch_pin_transitions(board.GP3))
    await asyncio.gather(interrupt_task1)

asyncio.run(main())