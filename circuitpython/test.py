from board import *
from time import sleep

import board
import audiomp3
import audiobusio
import audiomixer
import time 
import busio
import sdcardio
import storage
import os
import gc
from digitalio import DigitalInOut, Pull
import keypad
import displayio
# import terminalio
# import time                               
# import asyncio

# from lib.adafruit_debouncer import Debouncer

import adafruit_ssd1306

def get_display():
    displayio.release_displays()
    # This RPi Pico way to call I2C
    i2c = busio.I2C(scl=board.GP15, sda=board.GP14, frequency=200_000)
    return adafruit_ssd1306.SSD1306_I2C(128, 64, i2c)

display = get_display()
display.rotate(180)
y = 0
for x in range(7):
    y +=10
    display.text(f"testg {x}", 1, y, 1)
display.show()