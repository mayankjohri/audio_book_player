// #include <Arduino.h>

#include <AudioFileSourceBuffer.h>
#include <AudioFileSourceSD.h>
#include "AudioFileSourceID3.h"
#include <AudioGeneratorMP3.h>
#include <AudioOutputI2SNoDAC.h>
// #include <AudioOutputI2S.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>
#include <SD.h>
File root;
#define cspin 9


#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels
//set this to a value between 0 and 4095 to raise/lower volume
#define VOLUME_MAX 4095
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)

#define OLED_RESET -1        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C  ///< See datasheet for Address; 0x3C

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire1, OLED_RESET);

// const char *filename = "/audio_books/HP-and-the-prisoner-of-azkaban/HP-and-the-prisoner-of-azkaban-part1.mp3";
// const char *filename = "/audio_books/AP and the Philosoper stone/HP-and-the-philosopher-stone-audiobook-ch-1.mp3";
const char *filename = "/happy.mp3";
AudioGeneratorMP3 *mp3;
AudioFileSourceSD *file;
AudioOutputI2S *out;
AudioFileSourceID3 *startup_id3;
AudioFileSourceBuffer *buff;

File dataFile;
// AudioFileSourceFS file;
// Adafruit_MP3 player;
int volumn = 100;

// Called when a metadata event occurs (i.e. an ID3 tag, an ICY block, etc.
void MDCallback(void *cbData, const char *type, bool isUnicode, const char *string)
{
  const char *ptr = reinterpret_cast<const char *>(cbData);
  (void) isUnicode; // Punt this ball for now
  // Note that the type and string may be in PROGMEM, so copy them to RAM for printf
  char s1[32], s2[64];
  strncpy_P(s1, type, sizeof(s1));
  s1[sizeof(s1)-1]=0;
  strncpy_P(s2, string, sizeof(s2));
  s2[sizeof(s2)-1]=0;
  Serial.printf("METADATA(%s) '%s' = '%s'\n", ptr, s1, s2);
  Serial.flush();
}

// Called when there's a warning or error (like a buffer underflow or decode hiccup)
void StatusCallback(void *cbData, int code, const char *string)
{
  const char *ptr = reinterpret_cast<const char *>(cbData);
  // Note that the string may be in PROGMEM, so copy it to RAM for printf
  char s1[64];
  strncpy_P(s1, string, sizeof(s1));
  s1[sizeof(s1)-1]=0;
  Serial.printf("STATUS(%s) '%d' = '%s'\n", ptr, code, s1);
  Serial.flush();
}


void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial)
    ;                // Waiting for Serial Monitor
  Wire1.setSDA(14);  // Add these lines
  Wire1.setSCL(15);
  Wire1.begin();  //


  Serial.print("Initializing SD card...");
  SPI1.setRX(8);
  SPI1.setTX(11);
  SPI1.setSCK(10);
  SPI1.setCS(cspin);
  if (!SD.begin(cspin, SPI1)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  root = SD.open("/");
  Serial.println("0. Starting ..");

  Serial.println("0.1. Starting ..");


  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }

  display.flush();
  display.clearDisplay();
  display.setRotation(2);
  // display.invertDisplay(true);
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(20, 30);
  display.println("1. Starting ..");
  display.display();
  // delay(2000);
  // player.begin();

  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(20, 30);

  // display.setRotation(1);
  display.println("2. Namaskaram !");
  display.display();
  //  .begin();
  // file = new AudioFileSourceSD(filename);
  // out = new AudioOutputI2SNoDAC();
  // mp3 = new AudioGeneratorMP3();
  // AudioFileSourceSD.begin();
  file = new AudioFileSourceSD(filename);
  out = new AudioOutputI2SNoDAC();
  mp3 = new AudioGeneratorMP3();
  mp3->begin(file, out);
  mp3->loop();
  // // Startup File
  // file        = new AudioFileSourceSD(filename);
  // startup_id3         = new AudioFileSourceID3(file);
  // startup_id3->RegisterMetadataCB(MDCallback, (void *)"ID3TAG");
  // out = new AudioOutputI2S();
  // // out->SetPinout(22, 21, 25);
  // mp3 = new AudioGeneratorMP3();
  // mp3->RegisterStatusCB(StatusCallback, (void *)"mp3");
  // // Play the startup file right away
  // mp3->begin(startup_id3, out);

  // buff = new AudioFileSourceBuffer(file, 4096);
  // buff->RegisterStatusCB(StatusCallback, (void*)"buffer");
  // out = new AudioOutputI2SNoDAC();
  // mp3 = new AudioGeneratorMP3();
  // mp3->RegisterStatusCB(StatusCallback, (void*)"mp3");
  // mp3->begin(buff, out);
  // out->SetGain(2000);
  // dataFile = SD.open(filename);
  // if (!dataFile) {
  //   Serial.println("could not open file!");
  //   while (1)
  //     ;
  // }
  Serial.println("file was successfully opened!");
  print_me("3. Playing");
  delay(4);
  // //do this when there are samples ready
  // player.setSampleReadyCallback(writeDacs);

  // //do this when more data is required
  // player.setBufferCallback(getMoreData);

  // player.play();
  print_me("4. Playing ... ");
}




void writeDacs(int16_t l, int16_t r) {
  uint16_t vall = map(l, -32768, 32767, 0, VOLUME_MAX);
  uint16_t valr = map(r, -32768, 32767, 0, VOLUME_MAX);
  analogWrite(1, vall);
  analogWrite(1, valr);

}

int getMoreData(uint8_t *writeHere, int thisManyBytes) {
  int bytesRead = 0;
  int toRead = min(thisManyBytes, 768);  //limit the number of bytes we can read at a time so the file isn't interrupted
  while (dataFile.available() && bytesRead < toRead) {
    *writeHere = dataFile.read();
    writeHere++;
    bytesRead++;
  }
  return bytesRead;
}

void print_me(const char *txt) {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(20, 30);
  // Serial.println(txt);
  // display.setRotation(1);
  display.println(txt);
  display.display();
  display.clearDisplay();
}


void loop() {
  static int lastms = 0;

  if (mp3->isRunning()) {
    Serial.println("Running");
    if (millis()-lastms > 1000) {
      lastms = millis();
      Serial.printf("Running for %d ms...\n", lastms);
      Serial.flush();
     }
    // if (!mp3->loop()) mp3->stop();
  } else {
    Serial.printf("MP3 done\n");
    delay(1000);
    mp3->loop();
  }
}


// // void printDirectory(File dir, int numTabs) {
// //   while (true) {

// //     File entry =  dir.openNextFile();
// //     if (! entry) {
// //       // no more files
// //       break;
// //     }
// //     for (uint8_t i = 0; i < numTabs; i++) {
// //       Serial.print('\t');
// //     }
// //     Serial.println(entry.name());
// //     if (entry.isDirectory()) {
// //       Serial.println("/");
// //       printDirectory(entry, numTabs + 1);
// //     } else {
// //       // files have sizes, directories do not
// //       // Serial.print("\t\t");
// //       // Serial.print(entry.size(), DEC);
// //       // time_t cr = entry.getCreationTime();
// //       // time_t lw = entry.getLastWrite();
// //       // struct tm * tmstruct = localtime(&cr);
// //       // Serial.printf("\tCREATION: %d-%02d-%02d %02d:%02d:%02d", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
// //       // tmstruct = localtime(&lw);
// //       // Serial.printf("\tLAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
// //     }
// //     entry.close();
// //   }
// // }
