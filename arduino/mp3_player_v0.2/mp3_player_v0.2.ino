#include <arm_math.h>

#include <Adafruit_MP3.h>
#include <assembly.h>
#include <coder.h>
#include <mp3common.h>
#include <mp3dec.h>
#include <mpadecobjfixpt.h>
#include <statname.h>


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>
#include <SD.h>
File root;
#define cspin 9

#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels
//set this to a value between 0 and 4095 to raise/lower volume
#define VOLUME_MAX 1000
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)

#define OLED_RESET -1        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C  ///< See datasheet for Address; 0x3C

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire1, OLED_RESET);

const char *filename = "/happy.mp3";

File dataFile;
Adafruit_MP3 player;


void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial)
    ;                // Waiting for Serial Monitor
  Wire1.setSDA(14);  // Add these lines
  Wire1.setSCL(15);
  Wire1.begin();  //


  Serial.print("Initializing SD card...");
  SPI1.setRX(8);
  SPI1.setTX(11);
  SPI1.setSCK(10);
  SPI1.setCS(cspin);
  if (!SD.begin(cspin, SPI1)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  root = SD.open("/");
  Serial.println("0. Starting ..");

  Serial.println("0.1. Starting ..");


  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }

  display.flush();
  display.clearDisplay();
  display.setRotation(2);
  // display.invertDisplay(true);
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(20, 30);
  display.println("1. Starting ..");
  display.display();
  // delay(2000);
  player.begin();

  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(20, 30);

  // display.setRotation(1);
  display.println("2. Namaskaram !");
  display.display();
  dataFile = SD.open(filename);
  if (!dataFile) {
    Serial.println("could not open file!");
    while (1)
      ;
  }
  Serial.println("file was successfully opened!");
  print_me("3. Playing");
  delay(4);
  // //do this when there are samples ready
  player.setSampleReadyCallback(writeDacs);

  // //do this when more data is required
  player.setBufferCallback(getMoreData);

  player.play();
  print_me("4. Playing ... ");
}




void writeDacs(int16_t l, int16_t r) {
  uint16_t vall = map(l, -32768, 32767, 0, VOLUME_MAX);
  uint16_t valr = map(r, -32768, 32767, 0, VOLUME_MAX);
  analogWrite(1,vall);
  analogWrite(1,valr);
// #if defined(__SAMD51__)  // feather/metro m4
//   analogWrite(A0, vall);
//   analogWrite(A1, valr);
// #elif defined(__MK66FX1M0__) || defined(__MK64FX512__)  // teensy 3.6 or 3.5
//   analogWrite(A21, vall);
//   analogWrite(A22, valr);
// #elif defined(NRF52)
//   analogWrite(27, vall);
// #elif defined(__MK20DX256__)  //teensy 3.2
//   analogWrite(A14, vall);  //this board only has one dac, so play only left channel (or mono)
// #endif
}

int getMoreData(uint8_t *writeHere, int thisManyBytes) {
  int bytesRead = 0;
  int toRead = min(thisManyBytes, 768);  //limit the number of bytes we can read at a time so the file isn't interrupted
  while (dataFile.available() && bytesRead < toRead) {
    *writeHere = dataFile.read();
    writeHere++;
    bytesRead++;
  }
  return bytesRead;
}

void print_me(const char* txt) {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(20, 30);
  // Serial.println(txt);
  // display.setRotation(1);
  display.println(txt);
  display.display();
  display.clearDisplay();
}


void loop() {

  print_me(" 4.2. Playing");
  int x = player.tick();
  Serial.println(x);
  // while player
  Serial.println("completed");
  print_me("5. Playing completed");
  // datafile.close();
  delay(1000);
  // nothing happens after setup finishes.

  // Serial.println("inloop");
}


// // void printDirectory(File dir, int numTabs) {
// //   while (true) {

// //     File entry =  dir.openNextFile();
// //     if (! entry) {
// //       // no more files
// //       break;
// //     }
// //     for (uint8_t i = 0; i < numTabs; i++) {
// //       Serial.print('\t');
// //     }
// //     Serial.println(entry.name());
// //     if (entry.isDirectory()) {
// //       Serial.println("/");
// //       printDirectory(entry, numTabs + 1);
// //     } else {
// //       // files have sizes, directories do not
// //       // Serial.print("\t\t");
// //       // Serial.print(entry.size(), DEC);
// //       // time_t cr = entry.getCreationTime();
// //       // time_t lw = entry.getLastWrite();
// //       // struct tm * tmstruct = localtime(&cr);
// //       // Serial.printf("\tCREATION: %d-%02d-%02d %02d:%02d:%02d", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
// //       // tmstruct = localtime(&lw);
// //       // Serial.printf("\tLAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
// //     }
// //     entry.close();
// //   }
// // }
