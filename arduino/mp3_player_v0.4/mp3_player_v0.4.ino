// #include <M5Core2.h>
// #include <driver/i2s.h>
#include "AudioFileSourceSD.h"
// #include "AudioGeneratorWAV.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"

// #include "VButton.h"


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>
#include <SD.h>
File root;
#define cspin 9
#define I2S_NUM_0 0

#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

#define OLED_RESET -1        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C  ///< See datasheet for Address; 0x3C

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire1, OLED_RESET);



// AudioGeneratorWAV *wav = NULL;
AudioGeneratorMP3 *mp3 = NULL;
AudioFileSourceSD *file;
AudioOutputI2S *out;

#define BCLK_PIN 0
#define LRCK_PIN 1
#define SADTA_PIN 2
// #define BCLK_PIN 26
// #define LRCK_PIN 27
// #define SADTA_PIN 28


#define EXTERNAL_I2S 1
#define OUTPUT_GAIN 10

// VButton * button_bach;
// VButton * button_beethoven;
// VButton * button_chopin;

// uint16_t getColor(uint8_t red, uint8_t green, uint8_t blue){
//   return ((red>>3)<<11) | ((green>>2)<<5) | (blue>>3);
// }
// const char *path = "/happy.mp3";
const char *path = "/audio_books/HP-and-the-prisoner-of-azkaban/HP-and-the-prisoner-of-azkaban-part1.mp3";
// void button_callback(char *name, bool use_toggle, bool is_toggled){
// 	char path[128];


// 	if(mp3 != NULL){
// 		mp3->stop();
// 	}

// 	const char *path = "/happy.mp3";
// 	file = new AudioFileSourceSD(path);
// 	out = new AudioOutputI2S(I2S_NUM_0, EXTERNAL_I2S);
// 	out->SetPinout(a, LRCK_PIN, SADTA_PIN);
// 	out->SetOutputModeMono(true);
// 	out->SetGain((float)OUTPUT_GAIN/100.0);
// 	// wav = new AudioGeneratorWAV();
//   mp3 = new AudioGeneratorMP3();
// 	mp3->begin(file, out);
// }

void setup() {

  Serial.begin(115200);
  // while (!Serial)
  //   ;                // Waiting for Serial Monitor
  Wire1.setSDA(14);  // Add these lines
  Wire1.setSCL(15);
  Wire1.begin();  //


  Serial.print("Initializing SD card...");
  SPI1.setRX(8);
  SPI1.setTX(11);
  SPI1.setSCK(10);
  SPI1.setCS(cspin);
  if (!SD.begin(cspin, SPI1)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  root = SD.open("/");
  Serial.println("0. Starting ..");

  Serial.println("0.1. Starting ..");


  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }
  print_me("1. Starting ..");



  run();
}


void print_me(const char *txt) {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(20, 30);
  // Serial.println(txt);
  // display.setRotation(1);
  display.println(txt);
  display.display();
  display.clearDisplay();
}


void run() {
  print_me("Strating another song");
  if (mp3 != NULL) {
    mp3->stop();
  }


  file = new AudioFileSourceSD(path);
  out = new AudioOutputI2S();  // I2S_NUM_0, EXTERNAL_I2S);
  out->SetRate(44.1);
  out->SetPinout(BCLK_PIN, 1, 2);
  out->SetOutputModeMono(true);
  out->SetGain((float)OUTPUT_GAIN / 100.0);
  
  mp3 = new AudioGeneratorMP3();
  mp3->begin(file, out);
  print_me("2. Running");
}

void loop() {
  // button_bach->loop();
  // button_beethoven->loop();
  // button_chopin->loop();
  // Serial.println(mp3->isRunning());


  if (mp3 != NULL) {
    if (mp3->isRunning()) {
      if (!mp3->loop()) mp3->stop();
    } else {
      Serial.printf("mp3 done\n");
      print_me("3. mp3 done");
      delay(1000);
      run();
    }
  }
}